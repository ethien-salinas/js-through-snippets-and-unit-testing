import { allEqual } from '../array';

test('allEqual', () => {
    expect(allEqual([1, 2, 3, 4, 5, 6])).toBe(false);
    expect(allEqual([7, 7, 7, 7, 7, 7, 7])).toBe(true);
});
